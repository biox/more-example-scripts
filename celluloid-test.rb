require 'celluloid/current'
require 'parallel'
require 'pry'

filename = ARGV[0]
selector = ARGV[1].to_i

result = []
resultmin = 0

# Make chunk
class File
  def each_chunk(chunk_size = 4096)
    yield read(chunk_size) until eof?
  end
end

open(filename) do |f|
  Parallel.each(f.each_chunk, top_threads: 2) do |chunk|
    puts chunk.class
#    chunk.each_line do |line|
#      if line.to_i < resultmin
#      else
#        result << l.to_i
#        result = result.max(selector)
#        resultmin = result.min.to_i
#      end
#    end
  end
end

puts result.sort.join(' ')
