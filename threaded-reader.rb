#!/usr/local/bin ruby
require 'thread'

filename = ARGV[0]
#selection = ARGV[1].to_i

class ThreadChunker
  def initialize(pool_size = 2)
    @pool_size = pool_size
    @pool = []
  end

  def process!(data, &block)
    Array(data).each_slice(@pool_size) do |slice|
      slice.each do |item|
        @pool << Thread.new{ block.call(item) }
      end
      @pool.map{ |p| p.join }
      @pool = []
    end
  end
end

if true
  require 'pp'
  require 'benchmark'

  size = 2
  File.open(filename) do |f|
    list = []
    chunker = ThreadChunker.new(size)

    puts "Starting (Pool: %d Size: %d)" % [size, f.size]

    b = Benchmark.realtime{
      chunker.process!(f.each_line) do |line|
        b = Benchmark.realtime{
          list << line.chomp
          sleep rand*2
        }
        puts "\t\tFinished: #{line.chomp} -- %0.2f seconds" % b
      end
    }

    puts "Finished all: %0.2f seconds" % b
    puts "\nList: %s\n\n" % list.join(', ')
    puts list.class
    puts list.size
  end
end
