#!/usr/local/bin ruby

filename = ARGV[0]
selection = ARGV[1].to_i

result = Array.new
resultmin = 0

File.open(filename).each_line do |line|
  if line.to_i < resultmin
  else
    result << line.to_i
    result = result.max(selection)
    resultmin = result.min.to_i if result.size >= selection
  end
end

puts result.join(' ')
