#!/usr/local/bin ruby

## Yes I know this code is horrendous, I just wrote it quickly to test the parallel theory. Forgive me!
require 'celluloid/current'
require 'parallel'
require 'pry'

filename = ARGV[0]
@@selector = ARGV[1].to_i

@@result = []
@@resultmin = 0

# Make chunk
class File
  def each_chunk(chunk_size = 4096)
    yield read(chunk_size) until eof?
  end
end

# Parse chunk
class Parseme
  include Celluloid

  def parse(c)
    c.each_line do
      if line.to_i < resultmin
      else
        @@result << l.to_i
        @@result = @@result.max(@@selector)
        @@resultmin = @@result.min.to_i
      end
    end
  end
end

# pool = Parseme.new

# File.foreach(filename) do |line|
#  pool.future.parse(line)
# end

open(filename) do |f|
  Parallel.each(f.each_chunk, top_threads: 2) do |chunk|
    chunk.each_line do |line|
      puts line
    end
  end
end

# foreach chunk do |chunk|
#  chunk.each_line do |line|
#    parse(line)
#  end
# end
# pool.future.parse(chunk)

# puts @@result.sort.join(' ')
