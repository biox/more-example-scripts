POOL_SIZE = 10

jobs = Queue.new

10_0000.times{|i| jobs.push i}

workers = (POOL_SIZE).times.map do
  Thread.new do
    begin      
      while x = jobs.pop(true)
        puts "test forking #{x}"
      end
    rescue ThreadError
    end
  end
end

workers.map(&:join)
